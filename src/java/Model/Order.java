/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;


import java.io.Serializable;

public class Order implements Serializable{
	private int id;
	private AccountCustomer buyer;// nguoi mua
	private String buyDate;// ngay mua
	private double priceTotal;// tong tien

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AccountCustomer getBuyer() {
		return buyer;
	}

	public void setBuyer(AccountCustomer buyer) {
		this.buyer = buyer;
	}

	public String getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(String buyDate) {
		this.buyDate = buyDate;
	}

	public double getPriceTotal() {
		return priceTotal;
	}

	public void setPriceTotal(double priceTotal) {
		this.priceTotal = priceTotal;
	}
}