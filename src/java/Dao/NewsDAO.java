/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import Model.News;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author doanq
 */
public class NewsDAO extends DBContext{
    
     public List<News> getListNews() {
        List<News> data = new ArrayList<>();
        try {
            String strSelect = "select * from  News";
            PreparedStatement st = connection.prepareStatement(strSelect);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String title = rs.getString(2);
                String content = rs.getString(3);

                data.add(new News(id, title, content));
            }
        } catch (Exception e) {
            System.out.println("getListNews" + e.getMessage());
        }
        return data;
    }
      
       public News getNewsId(String Id) {
        String sql = "select * from News where news_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, Integer.parseInt(Id));
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                News c =  new News();
                c.setId(rs.getString(1));
                c.setTitle(rs.getString(2));
                c.setContent(rs.getString(3));
                c.setCreateDate(rs.getString(4));
                c.setModifired(rs.getString(5));
                c.setCreateBy(rs.getString(6));
                c.setModifiredBy(rs.getString(7));
                return c;
            }
        } catch (NumberFormatException | SQLException e) {
            System.out.println(e);
        }
        return null;
    }
       
        public static void main(String[] args) {
        NewsDAO cd = new NewsDAO();
        System.out.println(cd.getListNews());
        
    }
    
}
