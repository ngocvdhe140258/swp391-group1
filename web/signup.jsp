<%-- 
    Document   : signup
    Created on : May 21, 2023, 11:49:21 AM
    Author     : acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Created By CodingLab - www.codinglabweb.com -->
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <title> Responsive Registration Form | CodingLab </title>
        <link rel="stylesheet" href="login/css/signup.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <div class="title">Signup</div>
            <div class="content">
                <form action="signup" method="post">
                    <div class="user-details">
                        <div class="input-box">
                            <span class="details">Full Name</span>
                            <input type="text" placeholder="Enter your name" name="customer_name" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Username</span>
                            <input type="text" placeholder="Enter your username" name="username" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Email</span>
                            <input type="text" placeholder="Enter your email" name="email" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Phone Number</span>
                            <input type="text" placeholder="Enter your number" name="customer_phone" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Password</span>
                            <input type="password" placeholder="Enter your password" name="password" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Confirm Password</span>
                            <input type="password" placeholder="Confirm your password" name="repassword" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Address</span>
                            <input type="text" placeholder="Enter your address" name="customer_address" >
                        </div>
                        <div class="input-box">
                            <span class="details">Date of birth</span>
                            <input type="date" placeholder="" name="customer_dob" required>
                        </div>
                    </div>                  
                    <p class="alert-danger" style="color: red">
                        ${smess}
                    </p>
                    <div class="button">
                        <input type="submit" value="Signup">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
